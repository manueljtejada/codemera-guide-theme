# Codemera Guide Theme

Test project developed by Manuel J. Tejada for Codemera. Used the [Sage](https://roots.io/sage/) starter theme that's based on the HTML5 Boilerplate, Gulp, Bower and Bootstrap.

### Plugins used
* Very Simple Knowledge Base
* WP-Markdown
* Breadcrumb NavXT

### Additional Setup Information
* Page with slug 'guide' should be set as front page. Use `[knowledgebase-three]` to activate the Knowledge Base plugin.
* Create a menu with "Primary Navigation" set as the theme location. Add categories Programming, General and Company Policy to the Menu.

### Demo
A working, password-protected demo can be found at: [www.manueljtejada.com/codemera-guide](http://manueljtejada.com/codemera-guide)

* Password: elpollitopio
