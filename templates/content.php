<article <?php post_class(); ?>>
  <div class="row">
    <div class="col-sm-1 hidden-xs">
      <a href="<?php the_permalink(); ?>"><i class="fa fa-question-circle fa-2x"></i></a>
    </div>
    <div class="col-sm-11 col-xs-12">
      <header>
        <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      </header>
      <div class="entry-summary">
        <?php the_excerpt(); ?>
      </div>
    </div>
  </div>
</article>
