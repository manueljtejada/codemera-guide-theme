<footer class="content-info" role="contentinfo">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <p>&copy; Codemera, 2013. All rights reserved.</p>
  </div>
</footer>
