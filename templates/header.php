<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/logo.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <?php if (!is_page( 'guide' )) : ?>
        <!-- Search form for desktop -->
        <form role="search" method="get" class="navbar-form navbar-left hidden-xs hidden-sm" action="<?= esc_url(home_url('/')); ?>">
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
          <div class="form-group">
            <input type="search" value="<?= get_search_query(); ?>" name="s" class="form-control" placeholder="<?php _e('Search', 'sage'); ?>">
          </div>
        </form>
      <?php endif; ?>
      <!-- Form for mobile/tablet -->
      <form role="search" method="get" class="navbar-form navbar-left visible-xs visible-sm" action="<?= esc_url(home_url('/')); ?>">
        <div class="input-group">
          <input type="search" value="<?= get_search_query(); ?>" name="s" class="form-control" placeholder="<?php _e('Search', 'sage'); ?>">
          <div class="input-group-btn">
            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
          </div>
        </div>
      </form>
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav navbar-right']);
        endif;
      ?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<?php if ( is_page( 'guide' ) ) : ?>
  <div class="jumbotron">
    <div class="container text-center">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <h1>What are you looking for?</h1>
          <?php get_search_form(); ?>
        </div>
      </div>
    </div>
  </div>
<?php else : ?>
  <div class="breadcrumbs">
    <div class="container">
      <?php
          if(function_exists('bcn_display')) {
            bcn_display();
          }
      ?>
      <hr>
    </div>
  </div>
<?php endif; ?>
